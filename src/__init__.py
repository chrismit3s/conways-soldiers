SCALE_INIT = 100.0
SCALE_FACTOR = 0.9

SOLDIER_RADIUS = 0.4
# see coolors.co/0a141f-f5b700-0eb1d2-587674-f1fffa
PALETTE = {
    "background": (0x0A, 0x14, 0x1F),
    "soldier":    (0xF5, 0xB7, 0x00),
    "active":     (0x0E, 0xB1, 0xD2),
    "empty":      (0x58, 0x76, 0x74),
    "line":       (0xF1, 0xFF, 0xFA)}



from src.infinitegrid import InfiniteGrid
from src.game import Game
