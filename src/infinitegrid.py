import numpy as np


class InfiniteGrid():
    def __init__(self, empty_value=0, dtype="int"):
        self.empty = empty_value
        self.offset = np.zeros(shape=(2,), dtype="int")
        self.cells = np.full(shape=(1, 1), fill_value=self.empty, dtype=dtype)

    def __getitem__(self, key):
        # key doesnt contain any slices
        if all(isinstance(k, (int, np.integer)) for k in key):
            # tuple is needed to get around the extended indexing that happens when the index is a numpy array
            return self.cells[tuple(key + self.offset)] if self._in_range(key) else self.empty
        else:
            return self._get_view_of(key)

    def __setitem__(self, key, value):
        # key doesnt contain any slices
        if all(isinstance(k, (int, np.integer)) for k in key):
            self._resize_to_fit([key])
            self.cells[tuple(key + self.offset)] = value
        else:
            self._get_view_of(key)[:, :] = value

    def __iadd__(self, other):
        self.empty += other
        self.cells += other

    def __isub__(self, other):
        self.empty -= other
        self.cells -= other

    def __imul__(self, other):
        self.empty *= other
        self.cells *= other

    def __itruediv__(self, other):
        self.empty /= other
        self.cells /= other

    def __ifloordiv__(self, other):
        self.empty //= other
        self.cells //= other

    def __imod__(self, other):
        self.empty %= other
        self.cells %= other

    def _in_range(self, index):
        return ((-self.offset <= index) & (index < self.cells.shape - self.offset)).all()

    def _resize_to_fit(self, indices):
        indices = (*indices, -self.offset, self.cells.shape - self.offset - 1)  # also include top left and bottom right corner
        lower = xmin, ymin = np.min(indices, axis=0)
        upper = xmax, ymax = np.max(indices, axis=0) + 1

        # new resized cells array and offset
        new_cells = np.full(shape=upper-lower, fill_value=self.empty, dtype=self.cells.dtype)
        new_offset = -lower

        # copy cells
        fx, fy = new_offset - self.offset
        tx, ty = new_offset - self.offset + self.cells.shape
        new_cells[fx:tx, fy:ty] = self.cells[:, :]
        
        # replace attributes
        self.offset = new_offset
        self.cells = new_cells

    def _get_view_of(self, key):
        # turn every index into a slice object (and shift it by offset)
        key = list(key)
        for i, (k, o) in enumerate(zip(key, self.offset)):
            if isinstance(k, slice):
                if k.start is None or k.stop is None:
                    raise IndexError("Slice needs to have a starting and an ending point")
                # apply offset
                start = k.start + o
                stop = k.stop + o
                step = k.step
            elif isinstance(k, (int, np.integer)):
                # apply offset and turn indices into slices
                start = k + o
                stop = start + 1
                step = 1
            key[i] = slice(start, stop, step)

        # resize
        tl = np.array([x.start for x in key], dtype="int")
        br = np.array([x.stop for x in key], dtype="int")
        self._resize_to_fit([tl, br - 1])

        fx, fy = tl
        tx, ty = br
        sx, sy = tuple(x.step for x in key)
        return self.cells[fx:tx:sx, fy:ty:sx]
