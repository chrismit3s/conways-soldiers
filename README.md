# Conways Soldiers

An implementation of [Conway's Soldiers](https://en.wikipedia.org/wiki/Conway%27s_Soldiers).

![example video](./out/20200414T144431.webm)

## Setup

Just clone this repo, create a venv, install the packages and run it.

> _Note: You need python>=3.8, as this project uses the [walrus operator](https://docs.python.org/3/whatsnew/3.8.html#assignment-expressions)._

On linux:

    sudo apt update && sudo apt install python3.8-dev libsdl-ttf2.0-dev libsdl-mixer1.2-dev libsdl-image1.2-dev  # pygame deps
    git clone https://gitlab.com/chrismit3s/conways-soldiers
    cd conways-soldiers
    python3.8 -m venv venv
    source ./venv/bin/activate
    python -m pip install -r requirements.txt

On windows (with [py launcher](https://docs.python.org/3/using/windows.html) installed):

    git clone https://gitlab.com/chrismit3s/conways-soldiers
    cd conways-soldiers
    py -3.8 -m venv venv
    .\venv\scripts\activate.bat
    python -m pip install -r requirements.txt

Then just run the `src` module with:

    python -m src

## Controls

Leftclick toggle the cell under the cursor.
To jump a soldier over another, first select the soldier to jump with rightclick (should be highlighted blue now), and then select the soldier to jump over (again with rightclick).
The jump is executed immediately.
To move the canvas around, hold right click and drag.
