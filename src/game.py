from src import InfiniteGrid
from src import SCALE_INIT, SCALE_FACTOR, SOLDIER_RADIUS, PALETTE
import numpy as np
import pygame as pg
from pygame import gfxdraw
from pygame import draw


def _iter_indices(start, end):
    outer, inner = zip(start, end)
    for x in range(*sorted(outer)):
        for y in range(*sorted(inner)):
            yield np.array((x, y), dtype="int")


class Game():
    def __init__(self, res, fps):
        # initialize pygame (repeated calls have no effect)
        pg.init()

        # block uninteresting events
        pg.event.set_blocked([pg.ACTIVEEVENT, 
                              pg.JOYAXISMOTION, 
                              pg.JOYBALLMOTION, 
                              pg.JOYHATMOTION, 
                              pg.JOYBUTTONUP, 
                              pg.JOYBUTTONDOWN, 
                              pg.VIDEOEXPOSE, 
                              pg.USEREVENT])

        # display related
        self.fps = fps
        self.res = np.array(res)
        self.screen = pg.display.set_mode(res, pg.RESIZABLE)

        # for moving the screen, zooming, etc
        self.offset = self.res / 2
        self.scale = SCALE_INIT

        self.grid = InfiniteGrid()
        self.selected = None
        self.dragging = False
    
    @property
    def px_transform(self):
        """ helper property for Canvas.to_px and Canvas.from_px """
        return np.array((self.scale, -self.scale))

    def to_px(self, pos):
        """ from coordinates to pixel indices """
        return (pos * self.px_transform + self.offset).astype("int")

    def from_px(self, px):
        """ from pixel indices to coordinates """
        return (px - self.offset).astype("float") / self.px_transform

    def select(self, idx):
        if self.selected is None:  # nothing previously selected
            self.selected = idx
            return None
        elif (self.selected == idx).all():  # clicking the same soldier twice unselects it again
            self.selected = None
            return None
        else:  # one soldier already selected, another one clicked
            ret = (self.selected, idx)
            self.selected = None
            return ret

    def jump(self, a, b):
        dir = b - a
        c = a + 2 * dir

        # points are not adjacent
        if np.sum(np.abs(dir)) != 1:
            return

        # the index of the cell to be jumped to is occupied
        if self.grid[c] != 0:
            return

        self.grid[c] = 1
        self.grid[b] = 0
        self.grid[a] = 0

    def draw_circle(self, pos, r, color, filled=False):
        """
        draws a line around the coordinates pos with radius r (in units, not px)
        """
        px = self.to_px(pos)
        r *= self.scale
        color = PALETTE[color] if type(color) == str else color

        # for normal size cirlces, use antialiased gfx version
        if min(self.res) > r:
            args = (self.screen, *px, int(r), color)

            gfxdraw.aacircle(*args)
            if filled:
                gfxdraw.filled_circle(*args)

        # if it gets too big, gfxdraw acts out (see stackoverflow.com/q/5001070), then use the normal circle
        else:
            draw.circle(self.screen,
                        color,
                        px,
                        int(r),
                        (0 if filled else 1))  # width

    def draw_line(self, p1, p2, color):
        color = PALETTE[color] if type(color) == str else color
        draw.aaline(self.screen, color, self.to_px(p1), self.to_px(p2))

    def draw(self):
        self.screen.fill(PALETTE["background"])

        # draw all soldiers
        top_left = np.array((0, 0), dtype="int")
        bottom_right = self.res
        soldier_size = self.scale * SOLDIER_RADIUS

        start = np.ceil(self.from_px(top_left - soldier_size)).astype("int")
        end = np.ceil(self.from_px(bottom_right + soldier_size)).astype("int")

        for idx in _iter_indices(start, end):
            color = "empty"
            if self.grid[idx] != 0:
                color = "soldier"
            if (idx == self.selected).all():
                color = "active"

            self.draw_circle(pos=idx,
                             r=SOLDIER_RADIUS,
                             color=color,
                             filled=True)

        # draw the line
        line_y = self.to_px((0, -0.5))[1]
        self.draw_line(self.from_px((top_left[0], line_y)), self.from_px((bottom_right[0], line_y)), PALETTE["line"])

        pg.display.flip()

    def zoom(self, px, dir):
        """
        zooms the canvas towards the position px on screen; dir > 0 zooms in, dir < 0
        zooms out
        """
        factor = 1
        if dir > 0:
            factor = SCALE_FACTOR
        elif dir < 0:
            factor = 1 / SCALE_FACTOR

        self.scale *= factor
        self.offset = (self.offset - px) * factor + px

    def left_click(self, px):
        idx = self.from_px(px)
        closest = np.round(idx).astype("int")

        # if closest is close enough
        if np.linalg.norm(idx - closest) <= SOLDIER_RADIUS:
            # allow turning cells off always
            if self.grid[closest] != 0:
                self.grid[closest] = 0
            # allow turning cells on only below the line
            elif self.grid[closest] == 0 and closest[1] < 0:
                self.grid[closest] = 1

    def right_click(self, px):
        idx = self.from_px(px)
        closest = np.round(idx).astype("int")

        # if closest is close enough and closest is a soldier
        if np.linalg.norm(idx - closest) <= SOLDIER_RADIUS and self.grid[closest] != 0:
            if (pair := self.select(closest)) is not None:  # select clicked soldier
                self.jump(*pair)

    def tick(self):
        """
        ticks the game forwards, check events, mouse clicks, etc
        """
        done = False

        # check event queue
        for event in pg.event.get():
            # quit
            if event.type == pg.QUIT:
                done = True

            # window resized
            elif event.type == pg.VIDEORESIZE:
                self.res = np.array(event.size, dtype="int")
                self.screen = pg.display.set_mode(event.size, pg.RESIZABLE)

            # key pressed
            elif event.type == pg.KEYDOWN:
                # check for q or ctrl c
                if event.unicode == "q" or (event.key == pg.K_c and pg.key.get_mods() & pg.KMOD_CTRL):
                    done = True

            # mouse scroll zooms
            elif event.type == pg.MOUSEBUTTONDOWN and event.button in {pg.BUTTON_WHEELUP, pg.BUTTON_WHEELDOWN}:
                self.zoom(np.array(event.pos, dtype="float"),
                          1 if event.button == pg.BUTTON_WHEELUP else -1)

            # mouse click
            elif event.type == pg.MOUSEBUTTONDOWN and event.button == pg.BUTTON_LEFT:
                self.left_click(np.array(event.pos, dtype="float"))
                    
            # holding right click drags the canvas
            elif event.type == pg.MOUSEBUTTONDOWN and event.button == pg.BUTTON_RIGHT:
                self.right_click(np.array(event.pos, dtype="float"))
                self.dragging = True

            # right mouse button released => stop dragging
            elif event.type == pg.MOUSEBUTTONUP and event.button == pg.BUTTON_RIGHT:
                self.dragging = False
        
            # mouse moved => move canvas, if dragging
            elif event.type == pg.MOUSEMOTION and self.dragging:
                self.offset += event.rel

        # render
        self.draw()

        return done

    def loop(self):
        """
        the main loop; ticks forward until closed
        """
        while not self.tick():
            #sleep(1 / fps)
            pass
